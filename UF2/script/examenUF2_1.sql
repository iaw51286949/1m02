/*
iaw51286949
Ismael Antonio Aveiga Heras
*/

\c template1
DROP DATABASE IF EXISTS clubnautic;
CREATE DATABASE clubnautic;
\c clubnautic

-- 1
CREATE SEQUENCE soci_seq;

CREATE TABLE Patro(
    dni VARCHAR(9) CONSTRAINT dni_pk PRIMARY KEY,
    nom VARCHAR(100),
    adreca VARCHAR(50),
    telefon NUMERIC(9)
);

CREATE TABLE Soci(
    numSoci SMALLINT CONSTRAINT soci_pk PRIMARY KEY,
    dni VARCHAR(9),
    nom VARCHAR(100),
    adreca VARCHAR(50),
    telefon NUMERIC(9),
    dataAlta date DEFAULT CURRENT_DATE,
    ciutat VARCHAR(20)
);

CREATE TABLE Vaixell(
    matricula VARCHAR(7) CONSTRAINT matricula_pk PRIMARY KEY,
    soci SMALLINT,
    nomVaixell VARCHAR(100),
    numAmarre NUMERIC(2),
    quota NUMERIC(8,2),
    CONSTRAINT vaixell_soci_fk FOREIGN KEY(soci) REFERENCES SOCI(numSoci),
    CONSTRAINT vaixell_uk UNIQUE(nomVaixell)
);

CREATE TABLE Sortida(
    dataSortida date NOT NULL,
    vaixell VARCHAR,
    patro VARCHAR,
    dataRetorn date NOT NULL,
    destinacio VARCHAR(100),
    CONSTRAINT sortida_vaixell_fk FOREIGN KEY(vaixell) REFERENCES VAIXELL(matricula),
    CONSTRAINT sortida_patro_fk FOREIGN KEY(patro) REFERENCES Patro(dni)
);

INSERT INTO Patro VALUES
('12345678A','Erik','erick@gmail.com',698745123),
('98765432B','Pamela','pamela@gmail.com',623574891);

INSERT INTO Soci VALUES
(1,'74589612C','Nathaly','nathaly@gmail.com',624153789,DEFAULT,'Barcelona'),
(2,'84196375D','Brahian','brahian@gmail.com',658974123,DEFAULT,'Madrid');

SELECT SETVAL('soci_seq', (SELECT MAX(numSoci) FROM Soci), TRUE);

INSERT INTO Vaixell VALUES
('4715KR',1,'Titanic',1,1000.00),
('8751KDC',2,'Perla Negra',2,800.00);

