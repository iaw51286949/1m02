--iaw51286949
--Ismael Antonio Aveiga Heras

--3
select p.dni, p.nom, p.cognoms, p.dataalta, p.estudis, p.professio
from persona p join entrevista e on p.dni = e.dni
where lower(p.professio) = 'informatic';

--5
select p.dni DNI, nom ||' '|| cognoms "Nom_Cognoms", to_char(dataalta, 'yyyy') "Any"
from persona p join entrevista e on p.dni = e.dni;