-- Ismael Antonio Aveiga Heras

-- DML
-- 1 Insertar en la tabla DEPT un nuevo departamento de consultoria
-- que este ubicado en SANTANDER
INSERT INTO dept
VALUES (NEXTVAL('deptno_seq'),'CONSULTORIA','SANTANDER');

-- 2 Dar de alta un nuevo empleado de nombre Andreu, que ejercera de
-- analista en el departamento 30 y su jefe sera el empleado 7369. De
-- momento se desconoce el resto de datos
INSERT INTO emp
VALUES(NEXTVAL('empno_seq'),'ANDREU','ANALYST',7369,NULL,NULL,NULL,30);

-- 3 Cambia la fecha del empleado SCOTT por la de hoy
UPDATE emp
SET hiredate = CURRENT_DATE
WHERE empno=7788;

-- 4 El empleado MILLER, a causa de sus exitos es ascendido a lugar de
-- analista, aumentado su sueldo en un 20%, se le cambia al departamento
-- 30 y su nuevo jefe sera sera el empleado 7566
UPDATE emp
SET sal = sal+sal*0.20, job = 'ANALYST', deptno = 30, mgr = 7566
WHERE empno = 7934;

-- 5 A raiz de la firma del convenio anual de la empresa, se a determinado
-- incrementar el salario de todos los empleados en un 6%
UPDATE emp
SET sal=sal+sal*0.06;

-- 6 El empleado JAMES causa baja a la empresa
DELETE FROM emp
WHERE empno = 7900;

-- 7 Se contrata a SANZ, con numero 1657, para el departamento 30 y con
-- un salario 3000
INSERT INTO emp (empno,ename,sal,deptno)
VALUES (1657,'SANZ',3000,30);

-- 8 SANZ se cambia al departamento 40
UPDATE emp
SET deptno=40
WHERE empno=1657;

-- 9 SANZ trabaja de salesman, con una comision de 4000
UPDATE emp
SET job='SALESMAN', comm=4000
WHERE empno=1657;

-- 10 Se decide aumentar las comisiones. Aumenta todas las comisiones en
-- un 20% del salario
UPDATE emp
SET comm=com+comm*0.20;

-- 11
-- Para mas adelante

-- 12 Se despide a SANZ
DELETE emp
WHERE empno=1657;

-- 13 El departamento 30 desaparace. Se han despedir a todos los trabajadores
-- que trabajen en ella
DELETE FROM emp
WHERE deptno = 30;

DELETE FROM dept
WHERE deptno = 30;

-- 14
-- Para mas adelante


-- CONSULTAS BASICAS
/*
15 Mostar el codigo de empleado, salario, comision, numero de departamento y fecha de la
tabla EMP
*/
SELECT empno, sal, comm, deptno, hiredate
FROM emp;

/*
16 Mostrar toda la informacion de la tabla departamento
*/
SELECT *
FROM dept;

/*
17 Mostrar el nombre y la ocupacion de aquellos que sean SALESMAN
*/
SELECT ename, job
FROM emp
WHERE initcap(job)='Salesman';

/*
18 Mostrar el nombre y el codigo de departamento de aquellos empleados que no trabajen en el
departamento 30
*/
SELECT ename, deptno
FROM emp
WHERE deptno != 30;

/*
19 Mostrar los empleados que trabajan en el departamento 10 y 20 (las dos soluciones)
*/
-- Solucion 1
SELECT ename
FROM emp
WHERE deptno=10 OR deptno=20;

-- Solucion 2
SELECT ename
FROM emp
WHERE deptno IN (10,20);

/*
20 Mostrar el nombre y salario de aquellos empleados que gane mas de 2000
*/
SELECT ename, sal
FROM emp
WHERE sal > 2000;

/*
21 Mostrar el nombre y la fecha de contratacion de los empleados que han entrado antes del
1/1/82
*/
SELECT ename, hiredate
FROM emp
WHERE to_char(hiredate,'yyyy-mm-dd') < '1982-01-01';

/*
22 Mostrar el nombre de los SALESMAN que ganen mas de 1500
*/
SELECT ename
FROM emp
WHERE initcap(job)='Salesman' AND sal > 1500;

/*
23 Mostrar el nombre de aquellos que sean 'Clerk' o trabajan en el departamento
30
*/
SELECT ename
FROM emp
WHERE initcap(job)='Clerk' OR deptno=30;

/*
24 Mostrar aquellos que se llamen 'SMITH', 'ALLENT' o 'SCOTT'
*/
SELECT ename
FROM emp
WHERE initcap(ename) IN ('Smith', 'Allen','Scott');

/*
25 Mostrar aquellos que no se llamen 'SMITH', 'ALLENT' o 'SCOTT'
*/
SELECT ename
FROM emp
WHERE initcap(ename) NOT IN ('Smith', 'Allen','Scott');

/*
26 Mostrar aquellos el salario este entre 2000 y 3000
*/
SELECT ename
FROM emp
WHERE sal BETWEEN 2000 AND 3000;

/*
27 Mostrar aquellos empleados que trabajan en el departamento 10 o 20
*/
SELECT *
FROM emp
WHERE deptno IN (10, 20);

/*
28 Mostrar aquellos empleados el nombre comience por 'A'
*/
SELECT *
FROM emp
WHERE ename LIKE 'A%';

/*
29 Mostrar aquellos empleados el nombre tenga como segunda letra una 'D'
*/
SELECT *
FROM emp
WHERE ename LIKE '_D%';

/*
30 Mostrar los diferentes departamentos que hay a la tabla EMP
*/
SELECT DISTINCT deptno
FROM emp;

/*
31 Mostrar el departamento y el trabajo de los empleados (evitando repeticiones)
*/
SELECT DISTINCT deptno, job
FROM emp;

/*
32 Mostrar aquellos empleados que hayan entrado en 1981
*/
SELECT ename
FROM emp
WHERE to_char(hiredate,'yyyy')='1981';

/*
33 Mostrar aquellos empleados que tienen comision, mostrando nombre y comision
*/
SELECT *
FROM emp
WHERE comm IS NOT NULL;

/*
34 Mostrar aquellos empleados que ganen mas de 1500, ordenados por ocupacion
*/
SELECT ename
FROM emp
WHERE sal > 1500
ORDER BY job;

/*
35 Calcular el salario anual a percibir por cada empleado (teniendo en cuenta 14 pagas)
*/
SELECT ename, sal*14
FROM emp;

/*
36 Mostrar el nombre de los empleados salario y el incremento del 15% del salario
*/
SELECT ename, (sal*1.15)-sal 
FROM emp;

/*
37 Mostrar el nombre de los empleados salario y el incremento del 15% del salario y el
salario aumentado un 15%
*/
SELECT ename, (sal*1.15)-sal, sal*1.15 
FROM emp;