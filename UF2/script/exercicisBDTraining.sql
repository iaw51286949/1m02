-- Ismael Antonio Aveiga Heras

/*
1
*/
SELECT *
FROM producto
WHERE exist BETWEEN 25 AND 40;

/*
2
*/
SELECT DISTINCT repcod
FROM pedido;

/*
3
*/
SELECT *
FROM pedido
WHERE cliecod = 2111;

/*
4
*/
SELECT *
FROM pedido
WHERE cliecod = 2111 AND repcod = 103;

/*
5
*/
SELECT *
FROM pedido
WHERE cliecod = 2111 AND repcod = 103 AND lower(fabcod)='aci';

/*
6
*/
SELECT *
FROM pedido
ORDER BY cliecod, fecha ASC;

/*
7
*/
SELECT *
FROM repventa
WHERE ofinum IN (12,13);

/*
8
*/
SELECT *
FROM producto
WHERE exist = 0;

/*
9
*/
-- Primero se actualiza
UPDATE repventa
SET fcontrato= fcontrato + 5000;

-- Despues se hace la consulta
SELECT *
FROM repventa
WHERE to_char(fcontrato,'yyyy')='2003';

/*
10
*/
SELECT nombre, current_date - fcontrato "Dias contratados"
FROM repventa ;
