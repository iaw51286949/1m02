-- Ismael Antonio Aveiga Heras
-- Consultas multitabla
/*
1
*/
SELECT r.nombre, o.ciudad, o.region
FROM repventa r JOIN oficina o ON r.ofinum = o.ofinum;

/*
2
*/
SELECT p.pednum, p.importe, c.nombre, c.limcred
FROM pedido p JOIN cliente c ON p.cliecod = c.cliecod;

/*
3
*/
SELECT r.nombre, o.ofinum, o.ciudad, o.region
FROM repventa r JOIN oficina o ON r.ofinum = o.ofinum
ORDER BY r.nombre;

/*
4
*/
SELECT o.ciudad, o.objetivo, r.nombre, r.puesto
FROM oficina o JOIN repventa r ON o.director = r.repcod
WHERE objetivo > 360000;

/*
5
*/
SELECT pe.pednum, pe.importe, pr.descrip
FROM pedido pe JOIN producto pr ON pe.prodcod = pr.prodcod;

/*
6
*/
SELECT c.nombre "cliente", pe.pednum, pe.importe, pr.descrip, r.nombre "representante"
FROM cliente c JOIN pedido pe ON c.cliecod = pe.cliecod
JOIN producto pr ON pe.prodcod = pr.prodcod
JOIN repventa r ON pe.repcod = r.repcod 
WHERE pe.importe > 4000 
ORDER BY c.nombre, pe.importe DESC;

/*
SELECT c.nombre "cliente", pe.pednum, pe.importe, pr.descrip, r.nombre "representante"
FROM cliente c JOIN pedido pe ON c.cliecod = pe.cliecod
RIGHT JOIN producto pr ON (pr.prodcod, pr.fabcod) = (pe.prodcod, pe.fabcod)
JOIN repventa r ON r.repcod = pe.repcod 
WHERE pe.importe > 4000 
ORDER BY c.nombre, pe.importe DESC; 
*/

/*
7
*/
SELECT pe.pednum, pe.importe, c.nombre "cliente" ,r.nombre "representante"
FROM pedido pe JOIN cliente c ON pe.cliecod = c.cliecod
JOIN repventa r ON c.repcod = r.repcod
WHERE pe.importe > 2000;

/*
8
*/
SELECT pe.pednum, pe.importe, c.nombre "cliente", r.nombre "representante", o.ciudad
FROM pedido pe JOIN cliente c ON pe.cliecod = c.cliecod
JOIN repventa r ON c.repcod = r.repcod
JOIN oficina o ON r.ofinum = o.ofinum
WHERE pe.importe > 150;

/*
9
*/
SELECT pe.pednum, pe.importe, c.nombre, pe.fecha, pr.descrip
FROM pedido pe JOIN cliente c ON pe.cliecod = c.cliecod
JOIN producto pr ON pe.prodcod = pr.prodcod
WHERE to_char(fecha,'yyyy-mm') = '2003-10';

/*
10
*/
SELECT pe.pednum, pr.descrip, r.nombre
FROM pedido pe JOIN producto pr ON pe.prodcod = pr.prodcod
JOIN repventa r ON pe.repcod = r.repcod
JOIN oficina o ON r.ofinum = o.ofinum
WHERE lower(o.region)='este';

/*
11
*/
/*
SELECT pednum, importe, p.fecha
FROM pedido p LEFT JOIN repventa r ON r.fcontrato = p.fecha
WHERE p.fecha = r.fcontrato;
*/

/*
12
*/


/*
13
*/
SELECT r.nombre, r.ventas, o.ciudad
FROM repventa r JOIN oficina o ON r.ofinum = o.ofinum;

/*
14
*/


/*
15
*/
SELECT r.nombre
FROM repventa r JOIN repventa t ON r.jefe = t.repcod
WHERE r.cuota > t.cuota;

/*
16
*/
SELECT r.nombre, r.ofinum, t.nombre, t.ofinum
FROM repventa r JOIN repventa t ON r.jefe = t.repcod
WHERE r.ofinum != t.ofinum;

/*
17
*/


/*
18
*/
