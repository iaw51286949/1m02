\c template1
drop database if exists gabinet;
create database gabinet;
\c gabinet

CREATE TABLE CLIENT(
	DNI VARCHAR(9) CONSTRAINT client_dni_pk PRIMARY KEY,
	nom VARCHAR(20),
	adreca VARCHAR(40),
	telefon SMALLINT
	);

CREATE TABLE ASSUMPTE(
	numExpedient VARCHAR(6) CONSTRAINT assumpte_numexpedient_pk PRIMARY KEY,
	dataApertura DATE,
	dataTancament DATE,
	estat BOOL,
	dniClient VARCHAR(9),
	CONSTRAINT assumpte_dni_fk FOREIGN KEY (dniClient) REFERENCES CLIENT (dni)
	CONSTRAINT assumpte_estat_ck CHECK (estat = '0' OR estar = 'T')
	);

CREATE TABLE PROCURADOR(
	DNIprocurador VARCHAR(9) CONSTRAINT procurador_dni_pk PRIMARY KEY,
	nomProcurador VARCHAR(50),
	telefonProcurador SMALLINT
	);
	
CREATE TABLE AssumptexProcurador(
	numExpedient VARCHAR(6),
	dniProcurador VARCHAR(9),
	CONSTRAINT AssumptexProcurador_pk PRIMARY KEY (numExpedient, dniProcurador),
	CONSTRAINT AssumptexProcurador_numExpedient_fk FOREIGN KEY (numExpedient) 
				REFERENCES ASSUMPTE(numExpedient),
	CONSTRAINT AssumptexProcurador_dniProcurador_fk FOREIGN KEY (dniProcurador)
				REFERENCES PROCURADOR(dniProcurador)
);