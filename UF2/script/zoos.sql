\c template1
drop database if exists zoos;
\c zoos

CREATE TABLE ZOOS(
    idZoo VARCHAR(9) CONSTRAINT zoo_idZoo_PK PRIMARY KEY,
    nomZoo VARCHAR(40),
    Mida DECIMAL,
    Ciutat VARCHAR(20),
    PresupostAnual DECIMAL
);

CREATE TABLE ESPECIE(
    idEspecie VARCHAR(20) CONSTRAINT especie_idEspecie_pk PRIMARY KEY,
    nomVulgar VARCHAR(20),
    nomCientific VARCHAR(20),
    familia VARCHAR(20),
    perill BOOL
);

CREATE TABLE ANIMAL(
    idAnimal VARCHAR(6) CONSTRAINT animal_idAnimal_PK PRIMARY KEY,
    idZoo VARCHAR(9) CONSTRAINT animal_idZoo_pk PRIMARY KEY,
    sexe VARCHAR(15) CONSTRAINT animal_sexe_ck CHECK(sexe = 'macho' OR sexe = 'hembra' OR sexe = 'hemafrodita'),
    anyNeixement DATE,
    pais VARCHAR(20),
    continent VARCHAR(10),
    idEspecie VARCHAR(9),
    CONSTRAINT animal_especie_fk FOREIGN KEY (idEspecie) REFERENCES ESPECIE (idEspecie)
);