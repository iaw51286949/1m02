--- Ismael Antonio Aveiga Heras
--- iaw51286949

DROP SEQUENCE IF EXISTS cliecod_seq;
CREATE SEQUENCE cliecod_seq;
select setval('cliecod_seq', (select max(cliecod) from cliente), true);

-- FUNCIONS

-- 1
CREATE OR REPLACE FUNCTION existeixClient(p_cliecod INT)
RETURNS BOOLEAN
AS $$
BEGIN
    SELECT cliecod
    INTO STRICT p_cliecod
    FROM cliente
    WHERE cliecod = p_cliecod;
    IF p_cliecod IS NOT NULL THEN
    RETURN TRUE;
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
    RETURN FALSE;
END
$$LANGUAGE PLPGSQL;

-- 2
CREATE OR REPLACE FUNCTION altaClient(p_nombre VARCHAR, p_repcod INT, p_limcred NUMERIC(8,2))
RETURNS VARCHAR
AS $$
BEGIN
    INSERT INTO cliente VALUES(nextval('cliecod_seq'), p_nombre, p_repcod, p_limcred);
    RETURN 'Client ' ||p_nombre|| ' s"ha donat de alta';
END
$$LANGUAGE PLPGSQL;

-- 3
CREATE OR REPLACE FUNCTION stockOk(p_cant INT, p_fabcod VARCHAR, p_prodcod VARCHAR)
RETURNS BOOLEAN
AS $$
    DECLARE
        v_exist INT;

BEGIN
    SELECT COALESCE(exist,0)
    INTO STRICT v_exist
    FROM producto
    WHERE lower(fabcod || prodcod ) = lower(p_fabcod || p_prodcod);
    RETURN p_cant <= v_exist;
    --RETURN TRUE;
    
EXCEPTION
    WHEN NO_DATA_FOUND THEN
    RETURN FALSE;
END
$$LANGUAGE PLPGSQL;

-- 4
-- AUN NO ESTOY CAPACITADO

-- 5
CREATE OR REPLACE FUNCTION preuSenseIva(p_precio INT)
RETURNS NUMERIC
AS $$
    DECLARE
        v_senseIva INT;
BEGIN
    v_senseIva := COALESCE(p_precio/1.21,0);
    RETURN v_senseIva;
    
EXCEPTION
    WHEN NO_DATA_FOUND THEN
    RETURN FALSE;
END
$$LANGUAGE PLPGSQL;

-- 6
CREATE OR REPLACE FUNCTION preuAmbIva(p_precio INT)
RETURNS NUMERIC
AS $$
    DECLARE
        v_ambIva INT;
BEGIN
    v_ambIva := COALESCE(p_precio*1.21,0);
    RETURN v_ambIva;
    
EXCEPTION
    WHEN NO_DATA_FOUND THEN
    RETURN FALSE;
END
$$LANGUAGE PLPGSQL;