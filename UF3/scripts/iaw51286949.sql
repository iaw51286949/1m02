--- Ismael Antonio Aveiga Heras
--- iaw51286949

DROP SEQUENCE IF EXISTS seq_codclient;
CREATE SEQUENCE seq_codclient;
select setval('seq_codclient', (select max(codclient) from client));

DROP SEQUENCE IF EXISTS seq_numfra;
CREATE SEQUENCE seq_numfra;
select setval('seq_numfra', (select max(numfra) from factura));

DROP SEQUENCE IF EXISTS seq_numreserva;
CREATE SEQUENCE seq_numreserva;
select setval('seq_numreserva', (select max(numreserva) from reserva));

-- 1
DROP FUNCTION IF EXISTS afegirServei(INT, INT, DATE, INT);
CREATE OR REPLACE FUNCTION afegirServei(p_numhab INT, p_idservei INT, p_data date, p_quantitat INT)
RETURNS VARCHAR
AS $$
BEGIN
    INSERT INTO serveihabitacio VALUES(p_numhab, p_idservei, CURRENT_DATE, p_quantitat);
    RETURN 'Se a añadido el servicio' ;
END
$$LANGUAGE PLPGSQL;


-- 2
DROP FUNCTION IF EXISTS checkIn(VARCHAR);
CREATE OR REPLACE FUNCTION checkIn(p_dni VARCHAR(10))
RETURNS VARCHAR
AS $$
    DECLARE
        v_nom VARCHAR;
BEGIN
    SELECT nom
    INTO STRICT v_nom
    FROM client
    WHERE dni = p_dni;
    RETURN 'El nombre es ' || v_nom;
END$$
LANGUAGE PLPGSQL;


--- 3
/*
DROP FUNCTION IF EXISTS altaReserva(VARCHAR, DATE, DATE, VARCHAR. VARCHAR)
CREATE OR REPLACE FUNCTION altaReserva(p_dni VARCHAR, p_dataarribada DATE, p_datasortida DATE,p_codtipus VARCHAR p_codestada VARCHAR)
RETURNS VARCHAR
AS $$
BeGIN
END $$
LANGUAGE PLPGSQL;
*/