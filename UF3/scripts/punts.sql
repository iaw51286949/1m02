\c template1
DROP DATABASE IF EXISTS punts;
CREATE DATABASE punts;
\c punts

CREATE TABLE punts(
    id int CONSTRAINT id_pk PRIMARY KEY,
    valor int
);