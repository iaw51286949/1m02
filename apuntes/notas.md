# Apuntes clases

Ruta clase
/home/users/inf/wiaw2/iaw51286949/Downloads/



Ejecutar fichero para probar errores

psql -f [fichero] [nom_database]

Modos
gui = grafics user interface
cli = command line interface

Porcentaje
sal+sal*20/100
sal+sal*0.2
sal(1+0.2)
sal+(sal*6/100)
sal=1.06->1 es 100 y despues del punto es el porcentaje

Comandos
-lower: pasa a minuscula se usa en campo de BBDD
-upper: pasa a mayuscula se usa en campo de BBDD
-initcap: pasa la primera en mayuscula se usa en campo de BBDD
-distinc: sirve para que no se repita los campos
-to_char = to_char(campo,'formato')
-union: suma filas

Conversion:
coalesce evita errores de nulos 
-coalesce([campo], [nom_campo_sin_valor])
-::[format] = para conversion de datos
-coalesce(cast([campo] as [formato])

Apuntes
Un null no se puede ni operar ni comparar
En las concatenaciones para filas es comillas simple
Una condicion esta formada por un operador y dos operadores
SELECT, FROM y todo eso son clausulas
int->character y viceversa, character->date y viceversa
casting = conversion de un tipo de datos
% = metacaracteres

JOIN
suma campos
a <- b <- c (CA null)
c left join b left join a //a join b right c
outer join acepta nulls y falta fila

Vistas
\dv muestra relaciones?
\sv muestra codigo

Transaccions
Conjunto de operacions de DML que se a de ejecutar de manera conjunto, atomica (todo o nada)
BEGIN: inicio de transaccion
ROLLBACK: deshace los cambios
COMMIT: se aplica los cambios
SAVEPOINT: punto de guardado, conjunto a ROLLBACK para deshacer los cambios

Conexion remota:
psql -h "ip_server" -p "port" -U "usuari" "BBDD"

DCL
Grant: grant select on repventa to "altre usuari";
Revoke:
permisos a toda tabla: grant select on all tables in schema public to "altre_usuari";
ver permisos = \dp o \z
ver conectados a bbdd = select * from pg_stat_activity;

### Programacion en BBDD

Codigo:

```sql
-- Crear funcion
CREATE OR REPLACE FUNCTION helloWorld()
RETURNS tipus_de_dades
AS $$
[DECLARE
    variable local]
BEGIN
    RETURN 'Hola mundo';
END;
$$LANGUAGE PLPGSQL;


-- Llamar a la funcion
select helloWorld();
```

```sql
-- p_ : parametros
-- v_ : variables local
SELECT ename
INTO v_ename
FROM emp
WHERE emp = p_emp;
```

```sql
/* Crear la funcion infoUsuario donat el codi de empleat com a parametre
en mostrara el nom de l'empleat i el seu treball
*/
CREATE OR REPLACE FUNCTION infoUsuari(p_empno SMALLINT)
RETURNS VARCHAR
AS $$
    DECLARE
        v_ename VARCHAR;
        v_job VARCHAR;
BEGIN
    SELECT ename, job
    INTO v_ename, v_job
    FROM emp
    WHERE empno = p_empno;
    RETURN 'El nombre es ' ||v_ename|| ', treball de ' ||v_job;
END;
$$LANGUAGE PLPGSQL;
```

### Excepciones

- NO_DATA_FOUND

- TOO_MANY_ROWS

Ejemplo:

```sql
CREATE OR REPLACE FUNCTION infoUsuari(p_empno SMALLINT)
RETURNS VARCHAR
AS $$
    DECLARE
        v_ename VARCHAR;
        v_job VARCHAR;
BEGIN
    SELECT ename, job
    INTO STRICT v_ename, v_job
    FROM emp;
    --WHERE empno = p_empno;
    RETURN 'El nombre es ' ||v_ename|| ', treball de ' ||v_job;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
    RETURN 'El numero ' ||p_empno|| ' no existe';
    WHEN TOO_MANY_ROWS THEN
    RETURN 'Programador/a no esta capacitado';
END;
$$LANGUAGE PLPGSQL;
```

### Condiciones

Usando operadores AND, OR, NOT, =

```sql
IF conditions THEN
END IF;
```

```sql
-- salesman -> vendedor
-- clear -> oficinista
-- restos -> other
CREATE OR REPLACE FUNCTION infoUsuari(p_empno INT)
RETURNS VARCHAR
AS $$
    DECLARE
        v_ename VARCHAR;
        v_job VARCHAR;
BEGIN
    SELECT ename, job
    INTO STRICT v_ename, v_job
    FROM emp
    WHERE empno = p_empno;
    IF lower(v_job) = 'salesman' THEN
        v_job := 'vendedor';
    ELSIF lower(v_job) = 'clerk' THEN
        v_job := 'oficinista';
    ELSE
        v_job := 'other';
    END IF;
    RETURN 'El nombre es ' ||v_ename|| ', treball de ' ||v_job;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
    RETURN 'El numero ' ||p_empno|| ' no existe';
    WHEN TOO_MANY_ROWS THEN
    RETURN 'Programador/a no esta capacitado';
$$LANGUAGE PLPGSQL;
```